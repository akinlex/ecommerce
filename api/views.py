from store import models
# from categories.models import Category
from .serializers import ProductSerializer, OrderSerializer
from rest_framework import mixins, viewsets
# Create your views here.

class ProductViewSet(viewsets.ModelViewSet):
    queryset = models.Product.objects.all()
    print(queryset)
    serializer_class = ProductSerializer

class OrderViewSet(viewsets.ModelViewSet):
    queryset = models.Order.objects.all()
    serializer_class = OrderSerializer