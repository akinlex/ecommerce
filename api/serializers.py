from rest_framework import serializers
from store import models

class ProductSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = models.Product
        fields = ['id', 'name', 'slug', 'price', 'description', 'brand', 'model', 'category', 'color', 'shipping_fee',
         'digital', 'image', 'date_added' ]

class OrderSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = models.Order
        fields = ['id', 'customer', 'transaction_id', 'date_ordered', 'complete']