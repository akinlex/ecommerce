from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import ProductViewSet, OrderViewSet

router = DefaultRouter()
router.register('products', ProductViewSet, basename='products_api')
router.register('orders', OrderViewSet, basename='orders_api')

urlpatterns = router.urls