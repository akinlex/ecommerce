from django.urls import path
from . import views

# Set url patterns 
urlpatterns = [
    path('', views.store, name='store'),
    path('cart/', views.cart, name='cart'),
    path('checkout/', views.checkout, name='checkout'),
    path('update_item/', views.update_item, name='update_item'),
    path('process_order/', views.process_order, name='process_order'),
    path('transaction_completed/', views.process_order, name='transaction_completed'),
    path('<str:category>/<slug:slug>/', views.ProductDetailView.as_view(), name='product_detail')
]