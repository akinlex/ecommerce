import json
import datetime
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.views.generic import DetailView, ListView
from .models import Product, Order, OrderItem, ShippingAddress
from .utils import cookieCart, cartData, guestOrder

# Create your views here.
def store(request):
    data = cartData(request)

    cart_items = data['cart_items']
    order = data['order']
    items = data['items']

    # Get all products from db 
    products = Product.objects.order_by('date_added').reverse()
    context = {'products':products, 'cart_items':cart_items}
    return render(request, 'store.html', context)

class ProductDetailView(DetailView):
    model = Product
    template_name = 'product_detail.html'
    
    # def queryset(self):
    def get_context_data(self, **kwargs):
        data = cartData(self.request)

        context = super(ProductDetailView, self).get_context_data(**kwargs)
        product = get_object_or_404(Product, slug=self.kwargs['slug'], category=self.kwargs['category'])
        context['cart_items'] = data['cart_items']
        return context

def cart(request):
    data = cartData(request)

    cart_items = data['cart_items']
    order = data['order']
    items = data['items']

    context = {'items':items, 'order':order, 'cart_items':cart_items}
    return render(request, 'cart.html', context)

def checkout(request):
    data = cartData(request)

    cart_items = data['cart_items']
    order = data['order']
    items = data['items']

    context = {'items':items, 'order':order, 'cart_items':cart_items}
    return render(request, 'checkout.html', context)

def update_item(request):
    # Get json data(productID and action) passed from the frontend
    data = json.loads(request.body)
    product_id = data['productID']
    action = data['action']    

    customer = request.user.customer
    product = Product.objects.get(id=product_id)
    
    order, created = Order.objects.get_or_create(customer=customer, complete=False)
    
    order_item, created = OrderItem.objects.get_or_create(order=order, product=product)
    
    # Add or remove item based on user's action
    if action == 'add':
        order_item.quantity += 1
    elif action == 'remove':
        order_item.quantity -=1

    order_item.save()

    if order_item.quantity <=0:
        order_item.delete()        
    return JsonResponse('item added', safe=False)

def process_order(request):
    # Set transaction id using datetime timestamp, receive data from frontend
    # transaction_id = datetime.datetime.now().timestamp()
    data = json.loads(request.body)
    
    # Check if user is authenticated and process order
    if request.user.is_authenticated:
        customer = request.user.customer
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        total = float(data['form']['total'])
        order.transaction_id = str(data['form']['transaction_id'])
        print(order.transaction_id)
        
        # Check if total calculated is equal to total data sent from frontend
        if total == float(order.get_cart_total):
            order.complete = True
            print(order.complete)
        order.save()
        
        # Check if order needs to be shipped and update shipping address in db
        if order.shipping == True:
            ShippingAddress.objects.create(
                customer= customer,
                order = order,
                address = data['shipping']['address'],
                city = data['shipping']['city'],
                state = data['shipping']['state'],
                zipcode = data['shipping']['zipcode'],
                phone_number = data['shipping']['phone_number'],
                shipping_notes = data['shipping']['shipping_notes'],
            )
    else:
        print(f'User is not logged in')
    return JsonResponse('payment completed', safe=False)

