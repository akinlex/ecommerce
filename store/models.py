from PIL import Image
from django.db import models
from django.conf import settings
# from django.contrib.auth.models import User
from django.urls import reverse, reverse_lazy
from django.utils.text import slugify
from django.contrib.auth.models import AbstractUser, BaseUserManager

from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.

class MyUserManager(BaseUserManager):
	def create_user(self, email, password=None, **kwargs):
		"""
		Creates and saves a User with the given email, date of
		birth and password.
		"""
		if not email:
			raise ValueError('Users must have an email address')

		user = self.model(
			email=self.normalize_email(email),
			**kwargs
		)

		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_superuser(self, email, password=None, **kwargs):
		"""
		Creates and saves a superuser with the given email, date of
		birth and password.
		"""
		# kwargs.setdefault('is_staff', True)
		kwargs.setdefault('is_superuser', True)
		kwargs.setdefault('is_active', True)

		user = self.create_user(
			email,
			password=password,
			**kwargs
		)

		user.is_admin = True
		user.save(using=self._db)
		return user

class CustomUser(AbstractUser):
	email = models.EmailField(max_length=255, unique=True)
	username = None     
	is_active = models.BooleanField(default=True)
	is_admin = models.BooleanField(default=False)

	objects = MyUserManager()

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = []

	def __str__(self):
		return self.email
		
	def has_perm(self, perm, obj=None):
		"Does the user have a specific permission?"
		# Simplest possible answer: Yes, always
		return True

	def has_module_perms(self, app_label):
		"Does the user have permissions to view the app `app_label`?"
		# Simplest possible answer: Yes, always
		return True

	@property
	def is_staff(self):
		"Is the user a member of staff?"
		# Simplest possible answer: All admins are staff
		return self.is_admin

class Customer(models.Model):
	user = models.OneToOneField(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.CASCADE)
	first_name = models.CharField(max_length=200, null=True)
	last_name = models.CharField(max_length=200, null=True)
	
	# def __str__(self):
	# 	return self.last_name

class ProductCategory(models.Model):
	category_name = models.CharField(max_length=200)
	category_desc = models.TextField()

	def __str__(self):
		return self.category_name

class Product(models.Model):
	category = models.ForeignKey(ProductCategory, on_delete=models.CASCADE)
	name = models.CharField(max_length=200)
	price = models.DecimalField(decimal_places=2, max_digits=8)
	description = models.TextField()
	brand = models.CharField(max_length=200, null=True, blank=True)
	model = models.CharField(max_length=200, null=True, blank=True)
	color = models.CharField(max_length=200, null=True, blank=True)
	shipping_fee = models.DecimalField(decimal_places=2, max_digits=8, null=True, blank=True)
	digital = models.BooleanField(default=False,null=True, blank=True)
	image = models.ImageField(default='default.png', null=True, blank=True, upload_to='images')
	date_added = models.DateTimeField(auto_now_add=True)
	slug = models.SlugField(default='', editable=False, max_length=60)

	def __str__(self):
		return self.name
	
	# Save a/an product/item's slug
	def save(self, *args, **kwargs):        
		value = self.name
		self.slug = slugify(value, allow_unicode=True)
		super(Product, self).save(*args, **kwargs)

	# @property
	# def imageURL(self):
	#     try:
	#         url = self.image.url
	#     except:
	#         url = ''
	#     return url

class Order(models.Model):
	customer = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True, blank=True)
	date_ordered = models.DateTimeField(auto_now_add=True)
	complete = models.BooleanField(default=False)
	transaction_id = models.CharField(max_length=100, null=True)

	def __str__(self):
		return str(self.id)
	
	# Set shipping to false if product/item is a digital item
	@property
	def shipping(self):
		shipping = False
		orderitems = self.orderitem_set.all()
		for i in orderitems:
			if i.product.digital == False:
				shipping = True
		return shipping
	
	# Calculate total price of products and shipping fee per product in the customer's cart
	@property
	def get_cart_total(self):
	   orderitems = self.orderitem_set.all()
	   total = sum([item.get_total for item in orderitems])
	   shipping_fee_total = sum([item.get_shipping_fee for item in orderitems])
	   grand_total = total + shipping_fee_total
	   return grand_total
	
	# Calculate the total item in the customer's cart
	@property
	def get_cart_items(self):
		orderitems = self.orderitem_set.all()
		total = sum([item.quantity for item in orderitems])
		return total 

class OrderItem(models.Model):
	product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
	order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True)
	quantity = models.IntegerField(default=0, null=True, blank=True)
	date_added = models.DateTimeField(auto_now_add=True)
	
	# Calculate the total price of an item per quantity
	@property
	def get_total(self):
		if self.product.shipping_fee:
			total = self.product.price * self.quantity
			return total
		else:
			total = self.product.price * self.quantity
			return total

	# Get shipping fee for an item that has a shipping fee
	@property
	def get_shipping_fee(self):
		if self.product.shipping_fee:
			return self.product.shipping_fee
		else:
			shipping = 0
			return shipping

class ShippingAddress(models.Model):
	customer = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True)
	order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True)
	address = models.CharField(max_length=200, null=False)
	city = models.CharField(max_length=200, null=False)
	state = models.CharField(max_length=200, null=False)
	zipcode = models.CharField(max_length=200, null=False)
	phone_number = PhoneNumberField()
	shipping_notes = models.TextField(null=True, blank=True)
	date_added = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.address